import json
import time
import http.server

messages = []
message_id_counter = 0

class Handler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/get_messages":
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"server_timestamp": time.time(), "messages": messages}).encode("utf-8"))
        else:
            self.send_error(404)
    def do_POST(self):
        global message_id_counter
        if self.path == "/post_message":
            self.send_response(200)
            self.end_headers()
            message = json.loads(self.rfile.read(int(self.headers["Content-Length"])).decode("utf-8"))
            message["server_timestamp"] = time.time()
            message_id_counter += 1
            message["server_message_id"] = message_id_counter
            messages.append(message)
        else:
            self.send_error(404)
server_address = ('', 8000)
httpd = http.server.ThreadingHTTPServer(server_address, Handler)
httpd.serve_forever()
